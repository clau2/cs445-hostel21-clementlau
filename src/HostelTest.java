import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Test;


public class HostelTest {
	
	private Hostel hWithDeadline, hWithoutDeadline;
	private Availability a;
	private SearchResult s;
	private Room r;
	
	public HostelTest () {
		hWithDeadline = new Hostel();
		hWithoutDeadline = new Hostel();
		a = new Availability();
		s = new SearchResult();
		r = new Room(2);
		hWithDeadline.setCancellationDeadline(1000000);
		hWithDeadline.setCheckInTime(2200);
		hWithoutDeadline.setCancellationDeadline(0);
		hWithoutDeadline.setCheckInTime(2200);
		a.setDate(20140701);
		s.addAvailability(a);
	}

	@Test
	public void testAddRoomIfItDoesNotAlreadyExist() {
		assertNull(hWithDeadline.searchRoom(2));
		hWithDeadline.addRoomIfItDoesNotAlreadyExist(2);
		assertEquals(r, hWithDeadline.searchRoom(2));
	}

	@Test
	public void testSearchRoom() {
		hWithDeadline.addRoomIfItDoesNotAlreadyExist(1);
		assertEquals(new Room(1),hWithDeadline.searchRoom(1));
		assertNull(hWithDeadline.searchRoom(2));
	}

	@Test
	public void testIsTooLateToCancel() throws ParseException {
		assertTrue(hWithDeadline.isTooLateToCancel(s));
		assertFalse(hWithoutDeadline.isTooLateToCancel(s));
	}

}

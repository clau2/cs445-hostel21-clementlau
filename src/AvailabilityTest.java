import static org.junit.Assert.*;

import org.junit.Test;


public class AvailabilityTest {
	
	private Availability a;
	
	public AvailabilityTest () {
		a = new Availability();
		a.setDate(20140701);
	}

	@Test
	public void testReserve() {
		a.reserve();
		assertNull(a.search(20140701));
	}

	@Test
	public void testSearch() {
		assertEquals(a.search(20140701),a);
		assertNull(a.search(20140702));
	}

}

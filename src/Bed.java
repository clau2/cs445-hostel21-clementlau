import java.util.ArrayList;
import java.util.ListIterator;

public class Bed {
	private int id;
	private ArrayList <Availability> availableDates;
	
	public Bed () {
		this.id = 0;
		
		this.availableDates = new ArrayList <Availability> ();
	}
	
	public Bed ( int id ) {
		this.id = id;
		
		this.availableDates = new ArrayList <Availability> ();
	}
	
	public void setId ( int id ) {
		this.id = id;
	}
	
	public int getId () {
		return this.id;
	}
	
	public void addAvailability ( Availability availability ) {
		ListIterator <Availability> li = availableDates.listIterator();
		while ( li.hasNext() ) {
			Availability a = li.next();
			if ( availability.equals(a) ) {
				li.remove();
				this.availableDates.add(availability);
				return;
			}
		}
		this.availableDates.add(availability);
	}
	
	public SearchResult search ( int startDate, int endDate ) {
		boolean found;
		SearchResult result = new SearchResult();
		for ( int date = startDate; date < endDate; date = SearchEngine.nextDay(date) ) {
			found = false;
			ListIterator <Availability> li = availableDates.listIterator();
			while ( li.hasNext() && !found ) {
				Availability searchResult = li.next().search(date);
				if ( searchResult != null ) {
					result.addAvailability(searchResult);
					found = true;
				}
			}
			if ( !found ) {
				return null;
			}
		}
		return result;
	}
	
	public Availability checkAvailability ( int date ) {
		ListIterator <Availability> li = availableDates.listIterator();
		while ( li.hasNext() ) {
			Availability searchResult = li.next().search(date);
			if ( searchResult != null ) {
				return searchResult;
			}
		}
		return null;
	}
	
	public boolean equals ( Bed otherBed ) {
		return this.id == otherBed.getId();
	}
}

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;



public class XMLParser {
	private static File fXmlFile = null;
	private static DocumentBuilderFactory dbFactory;
	private static DocumentBuilder dBuilder;
	private static Document doc;
	private static Hostel21 h21;
	
	public static void init ( Hostel21 hostel21 ) throws SAXException, IOException, ParserConfigurationException {
		h21 = hostel21;
	}
	
	public static void openXML ( String fileName ) throws Exception {
		fXmlFile = new File(fileName);
		if (!fXmlFile.exists()) {
			throw new Exception(fileName + " doesn't exist");
	    }
		dbFactory = DocumentBuilderFactory.newInstance();
		dBuilder = dbFactory.newDocumentBuilder();
		doc = dBuilder.parse(fXmlFile);
	}
	
	public static void loadFromXML ( String fileName ) throws Exception {
		openXML(fileName);
		NodeList hostels = doc.getElementsByTagName("hostel");
		if ( hostels.getLength() == 0 ) {
			throw ( new Exception("Xml file has no hostels in it, or is formatted wrong") );
		}
		for ( int i = 0; i < hostels.getLength(); i++ ) {
			Element hostelElement = (Element) hostels.item(i);
			String city = hostelElement.getElementsByTagName("city").item(0).getTextContent();
			String name = hostelElement.getElementsByTagName("name").item(0).getTextContent();
			String address = hostelElement.getElementsByTagName("street").item(0).getTextContent();
			Hostel hostel = h21.addHostelIfItDoesNotAlreadyExists(city, name, address);
			hostel.setCancellationDeadline(Integer.parseInt(hostelElement.getElementsByTagName("cancellation_deadline").item(0).getTextContent()));
			hostel.setCancellationFee(Integer.parseInt(hostelElement.getElementsByTagName("cancellation_penalty").item(0).getTextContent().replaceAll("%", "")));
			hostel.setCheckInTime(Integer.parseInt(hostelElement.getElementsByTagName("check_in_time").item(0).getTextContent().replaceAll(":", "")));
			hostel.setCheckOutTime(Integer.parseInt(hostelElement.getElementsByTagName("check_out_time").item(0).getTextContent().replaceAll(":", "")));
			
			updateHostelFromXML(hostel,hostelElement);
		}
		System.out.println("done loading");
	}
	
	public static void updateHostelFromXML ( Hostel hostel , Element hostelElement ) {
		NodeList availabilities = hostelElement.getElementsByTagName("availability");
		for ( int i = 0; i < availabilities.getLength(); i++ ) {
			Element availabilityElement = (Element) availabilities.item(i);
			int date = Integer.parseInt(availabilityElement.getElementsByTagName("date").item(0).getTextContent());
			int price = Integer.parseInt(availabilityElement.getElementsByTagName("price").item(0).getTextContent());
			int roomId = Integer.parseInt(availabilityElement.getElementsByTagName("room").item(0).getTextContent());
			int bedId = Integer.parseInt(availabilityElement.getElementsByTagName("bed").item(0).getTextContent());
			Availability availability = new Availability(price,date,roomId,bedId);
			hostel.addAvailability(availability);
		}
	}
}

import java.util.ArrayList;
import java.util.ListIterator;


public class SearchResult {
	private ArrayList <Availability> results;
	private int price;
	private int id;
	private static int idCounter = 1;
	private Hostel hostel;
	private int num;
	private boolean booked;
	
	public SearchResult () {
		this.results = new ArrayList <Availability> ();
		this.price = 0;
		this.id = idCounter++;
		booked = false;
	}
	
	public void addAvailability ( Availability a ) {
		this.results.add(a);
		this.price += a.getPrice();
	}
	
	public void addAll ( SearchResult s ) {
		ListIterator <Availability> li = s.getResults().listIterator();
		while ( li.hasNext() ) {
			this.results.add(li.next());
		}
		this.price += s.getPrice();
	}
	
	public void cancel () {
		ListIterator <Availability> li = results.listIterator();
		while ( li.hasNext() ) {
			li.next().cancel();
		}
		booked = false;
	}
	
	public ArrayList <Availability> getResults () {
		return this.results;
	}
	
	public int getId () {
		return id;
	}
	
	public int getPrice () {
		return this.price;
	}
	
	public int getNum () {
		return num;
	}
	
	public Hostel getHostel () {
		return hostel;
	}
	
	public void setHostel ( Hostel h ) {
		hostel = h;
	}
	
	public void setNum ( int i ) {
		num = i;
	}
	
	public String toString () {
		if ( results.size() == 0 ) {
			return "This search returned no results.";
		}
		String x = "search_id:" + this.id + ", $" + this.price + ", ";
		ArrayList <Integer> rooms = new ArrayList <Integer> ();
		ListIterator <Availability> li = results.listIterator();
		while ( li.hasNext() ) {
			int p = li.next().getRoomId();
			if ( !rooms.contains(p) ) {
				rooms.add(p);
			}
		}
		if ( rooms.size() > 1 ) {
			x += "rooms ";
			ListIterator <Integer> li2 = rooms.listIterator();
			x += li2.next();
			while ( li2.hasNext() ) {
				x += ", " + li2.next();
			}
			return x;
		} else {
			x += "room " + rooms.get(0);
			return x;
		}
	}
	
	public boolean equals ( Object other ) {
		if ( !(other instanceof SearchResult) ) {
			return false;
		}
		SearchResult s = (SearchResult) other;
		System.out.println("a");
		if ( !results.containsAll(s.getResults()) ) {
			return false;
		}
		if ( !s.getResults().containsAll(results) ) {
			return false;
		}
		return true;
	}
	
	public void book ( Customer reservee ) throws Exception {
		if ( booked ) {
			throw new Exception("This search result is already booked.");
		}
		ListIterator <Availability> li = results.listIterator();
		while ( li.hasNext() ) {
			li.next().reserve();
		}
		booked = true;
	}
	
	public boolean isEmpty () {
		return results.isEmpty();
	}
	
	public int size () {
		return results.size();
	}
	
	public int maxPrice () {
		ListIterator <Availability> li = results.listIterator();
		int max = 0;
		while ( li.hasNext() ) {
			int p = li.next().getPrice();
			max = p > max ? p : max;
		}
		return max;
	}
	
	public int minPrice () {
		ListIterator <Availability> li = results.listIterator();
		int min = Integer.MAX_VALUE;
		while ( li.hasNext() ) {
			int p = li.next().getPrice();
			min = p < min ? p : min;
		}
		return min;
	}
	
	public int startDate () {
		ListIterator <Availability> li = results.listIterator();
		int min = Integer.MAX_VALUE;
		while ( li.hasNext() ) {
			int d = li.next().getDate();
			min = d < min ? d : min;
		}
		return min;
	}
	
	public int endDate () {
		ListIterator <Availability> li = results.listIterator();
		int max = 0;
		while ( li.hasNext() ) {
			int d = li.next().getDate();
			max = d > max ? d : max;
		}
		return SearchEngine.nextDay(max);
	}
}

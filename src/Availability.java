
public class Availability {
	private int price;
	private int date;
	private boolean available;
	
	private int roomId;
	private int bedId;
	
	public Availability () {
		this.price = 0;
		this.date = 0;
		this.available = true;
		
		this.roomId = 0;
		this.bedId = 0;
	}
	
	public Availability ( int price, int date, int roomId, int bedId ) {
		this.price = price;
		this.date = date;
		this.available = true;
		
		this.roomId = roomId;
		this.bedId = bedId;
	}
	
	public int getPrice () {
		return this.price;
	}
	
	public int getDate () {
		return this.date;
	}
	
	public int getRoomId () {
		return this.roomId;
	}
	
	public int getBedId() {
		return this.bedId;
	}
	
	public void setPrice ( int price ) {
		this.price = price;
	}
	
	public void setDate ( int date ) {
		this.date = date;
	}
	
	public void setRoomId ( int roomId ) {
		this.roomId = roomId;
	}
	
	public void setBedId ( int bedId ) {
		this.bedId = bedId;
	}
	
	public void reserve () {
		this.available = false;
	}
	
	public void cancel () {
		available = true;
	}
	
	public Availability search ( int date ) {
		if ( this.date == date && this.available ) {
			return this;
		} else {
			return null;
		}
	}
	
	public boolean equals ( Availability otherAvailability ) {
		return this.date == otherAvailability.getDate();
	}
	
	public String toString () {
		String returnValue = "Bed " + this.bedId + " in Room " + this.roomId + " is available on " + this.date + ".";
		return returnValue;
	}
}

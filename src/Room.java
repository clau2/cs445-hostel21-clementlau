import java.util.ArrayList;
import java.util.ListIterator;

public class Room {
	private ArrayList <Bed> beds;
	
	private int id;
	
	private Hostel hostel;
	
	public Room () {
		this.id = 0;
		this.beds = new ArrayList <Bed> ();
	}
	
	public Room ( int id ) {
		this.id = id;
		this.beds = new ArrayList <Bed> ();
	}
	
	public void addAvailability ( Availability availability ) {
		Bed bed = addBedIfItDoesNotAlreadyExist(availability.getBedId());
		bed.addAvailability(availability);
	}
	
	public Bed addBedIfItDoesNotAlreadyExist ( int id ) {
		ListIterator <Bed> li = beds.listIterator();
		while ( li.hasNext() ) {
			Bed bed = li.next();
			if ( bed.getId() == id ) {
				return bed;
			}
		}
		Bed newlyAddedBed = new Bed(id);
		beds.add(newlyAddedBed);
		return newlyAddedBed;
	}
	
	public Bed getBed ( int id ) {
		ListIterator <Bed> li = beds.listIterator();
		while ( li.hasNext() ) {
			Bed possibleResult = li.next();
			if ( possibleResult.equals(new Bed(id)) ) {
				return possibleResult;
			}
		}
		return null;
	}
	
	public void addBed ( Bed newBed ) {
		beds.add ( newBed );
	}
	
	public int getId () {
		return this.id;
	}
	
	public void setHostel ( Hostel h ) {
		hostel = h;
	}
	
	public ArrayList <SearchResult> search ( int startDate, int endDate ) {
		ListIterator <Bed> li = beds.listIterator();
		ArrayList <SearchResult> searchResults = new ArrayList <SearchResult> ();
		while ( li.hasNext() ) {
			SearchResult a = li.next().search(startDate, endDate);
			if ( a != null ) {
				searchResults.add(a);
			}
		}
		if ( !searchResults.isEmpty() ) {
			return searchResults;
		}
		return null;
	}
	
	public SearchResult checkAvailability ( int date ) {
		ListIterator <Bed> li = beds.listIterator();
		SearchResult results = new SearchResult();
		while ( li.hasNext() ) {
			Bed bed = li.next();
			Availability possibleAvailability = bed.checkAvailability(date);
			if ( possibleAvailability != null ) {
				results.addAvailability(possibleAvailability);
			}
		}
		results.setHostel(hostel);
		return results;
	}
	
	public boolean equals ( Object other ) {
		return other instanceof Room && id == ((Room)other).getId();
	}
}

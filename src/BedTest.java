import static org.junit.Assert.*;

import org.junit.Test;


public class BedTest {
	
	private Bed b1, b2;
	private Availability a1, a2;
	private SearchResult s1, s2;
	
	public BedTest () {
		b1 = new Bed();
		b2 = new Bed();
		a1 = new Availability(20,20140701,1,1);
		a2 = new Availability(20,20140702,1,1);
		s1 = new SearchResult();
		s2 = new SearchResult();
		b1.addAvailability(a1);
		b2.addAvailability(a1);
		b2.addAvailability(a2);
		s1.addAvailability(a1);
		s2.addAvailability(a1);
		s2.addAvailability(a2);
	}

	@Test
	public void testSearch () {
		assertEquals(s1,b1.search(20140701,20140702));
		assertEquals(s2,b2.search(20140701,20140703));
		
	}
	
	@Test
	public void testCheckAvailability () {
		assertNull(b1.checkAvailability(20140702));
		assertEquals(a1,b1.checkAvailability(20140701));
	}
	
	

}

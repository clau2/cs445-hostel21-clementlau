import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Calendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Hostel {
	private String city;
	private String name;
	private String address;
	private ArrayList <Room> rooms;
	private int cancellationFee;
	private int cancellationDeadline;
	private int checkInTime;
	private int checkOutTime;
	
	public Hostel () {
		this.city = null;
		this.name = null;
		this.address = null;
		
		this.rooms = new ArrayList <Room> (); 
	}
	
	public Hostel ( String city ) {
		this.city = city;
		this.name = null;
		this.address = null;
		
		this.rooms = new ArrayList <Room> ();
	}
	
	public Hostel ( String city, String name, String address ) {
		this.city    = city;
		this.name    = name;
		this.address = address;
		
		this.rooms = new ArrayList <Room> ();
	}
	
	public void setCancellationFee ( int a ) {
		cancellationFee = a;
	}
	
	public void setCancellationDeadline ( int a ) {
		cancellationDeadline = a;
	}
	
	public void setCheckInTime ( int a ) {
		checkInTime = a;
	}
	
	public void setCheckOutTime ( int a ) {
		checkOutTime = a;
	}
	
	public int getCancellationFee () {
		return cancellationFee;
	}
	
	public String getAddress () {
		return address;
	}
	
	public Room getRoom ( int id ) {
		ListIterator <Room> li = rooms.listIterator();
		while ( li.hasNext() ) {
			Room possibleResult = li.next();
			if ( possibleResult.equals(new Room(id)) ) {
				return possibleResult;
			}
		}
		return null;
	}
	
	public int getCheckInTime () {
		return checkInTime;
	}
	
	public int getCheckOutTime () {
		return checkOutTime;
	}
	
	public void addAvailability ( Availability availability ) {
		Room room = addRoomIfItDoesNotAlreadyExist(availability.getRoomId());
		room.addAvailability(availability);
	}
	
	public Room addRoomIfItDoesNotAlreadyExist ( int id ) {
		ListIterator <Room> li = rooms.listIterator();
		while ( li.hasNext() ) {
			Room room = li.next();
			if ( room.getId() == id ) {
				return room;
			}
		}
		Room newlyAddedRoom = new Room(id);
		newlyAddedRoom.setHostel(this);
		rooms.add(newlyAddedRoom);
		return newlyAddedRoom;
	}
	
	public String getCity () {
		return this.city;
	}
	
	public String getName () {
		return this.name;
	}
	
	public ArrayList <SearchResult> search ( int num, int startDate, int endDate ) throws Exception {
		ArrayList <SearchResult> searchResults = new ArrayList <SearchResult> ();
		ListIterator <Room> li = rooms.listIterator();
		while ( li.hasNext() ) {
			ArrayList <SearchResult> a = li.next().search(startDate, endDate);
			if ( a != null ) {
				searchResults.addAll(a);
			}
		}
		if ( searchResults.isEmpty() ) {
			return null;
		} 
		if ( num == 1 ) {
			return searchResults;
		}
		if ( searchResults.size() < num ) {
			throw new Exception("only " + searchResults.size() + " beds available over this duration");
		}
		while ( searchResults.size() > num ) {
			ListIterator <SearchResult> resultIterator = searchResults.listIterator();
			int greatestPrice = 0;
			SearchResult greatestResult = null;
			while ( resultIterator.hasNext() ) {
				SearchResult r = resultIterator.next();
				if ( r.getPrice() > greatestPrice ) {
					greatestPrice = r.getPrice();
					greatestResult = r;
				}
			}
			searchResults.remove(greatestResult);
		}
		SearchResult multipleBedResult = new SearchResult();
		ListIterator <SearchResult> resultIterator = searchResults.listIterator();
		while ( resultIterator.hasNext() ) {
			multipleBedResult.addAll(resultIterator.next());
			resultIterator.remove();
		}
		searchResults.add(multipleBedResult);
		return searchResults;
	}
	
	public SearchResult checkAvailability ( int date ) {
		ListIterator <Room> li = rooms.listIterator();
		SearchResult result = new SearchResult();
		while ( li.hasNext() ) {
			Room room = li.next();
			result.addAll(room.checkAvailability(date));
		}
		return result;
	}
	
	public Room searchRoom ( int id ) {
		ListIterator <Room> li = rooms.listIterator();
		while ( li.hasNext() ) {
			Room possibleResult = li.next();
			if ( possibleResult.equals(new Room(id)) ) {
				return possibleResult;
			}
		}
		return null;
	}
	
	public boolean equals ( Hostel otherHostel ) {
		return this.city.compareToIgnoreCase(otherHostel.getCity()) == 0;
	}
	
	public boolean isTooLateToCancel ( SearchResult s ) throws ParseException {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.HOUR_OF_DAY, cancellationDeadline);
		Calendar startDate = Calendar.getInstance();
		String start = String.valueOf(s.startDate());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
		startDate.setTime(sdf.parse(start+String.valueOf(checkInTime)));
		if ( c.after(startDate) ) {
			return true;
		}
		return false;
	}
}

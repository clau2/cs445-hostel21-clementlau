/* This is the handy dandy search engine that, given
 * a date range and number of rooms needed, will search
 * for suitable beds, and also provides the option of
 * reserving those beds.
 */

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ListIterator;


@SuppressWarnings("serial")
class SearchException extends Exception{
	public SearchException ( String message ) {
		super(message);
	}
}

public class SearchEngine {
	
	private static Hostel21 company;
	
	public static int todaysDate;
	
	private static ArrayList <SearchResult> resultList;
	
	public static void init ( Hostel21 h21 ) {
		company = h21;
		
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		todaysDate = Integer.parseInt(df.format(date));
		
		resultList = new ArrayList <SearchResult> ();
	}
	
	public static ArrayList <SearchResult> search ( String city, int num, int startDate, int endDate ) throws Exception {
		if ( !validateDate(startDate) ) {
			throw new SearchException("Invalid start date");
		}
		if ( !validateDate(endDate) ) {
			throw new SearchException("Invalid end date");
		}
		if ( startDate > endDate ) {
			throw new SearchException("Start date must be before end date");
		}
		Hostel hostel = searchHostel(city);
		ArrayList <SearchResult> results = hostel.search(num, startDate, endDate);
		resultList.addAll(results);
		ListIterator <SearchResult> li = results.listIterator();
		while ( li.hasNext() ) {
			SearchResult s = li.next();
			s.setNum(num);
			s.setHostel(hostel);
		}
		System.out.println(hostel.getName() + ", " + hostel.getCity());
		return results;
	}
	
	public static ArrayList <SearchResult> checkAvailability ( String city, int startDate, int endDate ) throws SearchException {
		if ( !validateDate(startDate) ) {
			throw new SearchException("Invalid start date");
		}
		if ( !validateDate(endDate) ) {
			throw new SearchException("Invalid end date");
		}
		if ( startDate > endDate ) {
			throw new SearchException("Start date must be before end date");
		}
		Hostel hostel = searchHostel(city);
		ArrayList <SearchResult> results = new ArrayList <SearchResult> ();
		for ( int date = startDate; date < endDate; date = nextDay(date) ) {
			results.add(hostel.checkAvailability(date));
		}
		return results;
	}
	
	public static int nextDay ( int date ) {
		int year = getYearFromDate(date);
		int month = getMonthFromDate(date);
		int day = getDayFromDate(date);
		int[] daysInMonth = {0,31,29,31,30,31,30,31,31,30,31,30,31};
		day ++;
		if ( day > daysInMonth[month] ) {
			day = 1;
			month ++;
		}
		if ( day == 29 && month == 2 && year%4 != 0 ) {
			day = 1;
			month ++;
		}
		if ( month == 13 ) {
			month = 1;
			year ++;
		}
		date = year * 10000 + month * 100 + day;
		return date;
	}
	
	public static boolean hasEnoughBedsInTheSameRoom ( Availability [][] results, int num ) {
		for ( int i = 0; i < results.length; i++ ) {
			Availability [] roomResults = results[i];
			if ( roomResults.length >= num ) {
				return true;
			}
		}
		return false;
	}
	
	
	
	public static boolean validateDate ( int date ) {
		if ( date < todaysDate ) {
			return false;
		}
		if ( getMonthFromDate(date) < 1 || getMonthFromDate(date) > 12 ) {
			return false;
		}
		if ( getDayFromDate(date) < 1 ) {
			return false;
		}
		int[] daysInMonth = {0,31,29,31,30,31,30,31,31,30,31,30,31};
		if ( getDayFromDate(date) > daysInMonth[getMonthFromDate(date)] ) {
			return false;
		}
		if ( getYearFromDate(date)%4 != 0 && getMonthFromDate(date) == 2 && getDayFromDate(date) == 29 ) {
			return false;
		}
		return true;
	}
	
	public static int getYearFromDate ( int date ) {
		return date/10000;
	}
	
	public static int getMonthFromDate ( int date ) {
		return (date/100)%100;
	}
	
	public static int getDayFromDate ( int date ) {
		return date%100;
	}
	
	public static Hostel searchHostel ( String in ) throws SearchException {
		Hostel possibleResult = company.searchHostel(in);
		if ( possibleResult != null ) {
			return possibleResult;
		}
		throw new SearchException("No hostel found in your city.");
	}
	
	public static SearchResult getResult ( int resultId ) {
		ListIterator <SearchResult> li = resultList.listIterator();
		while ( li.hasNext() ) {
			SearchResult result = li.next();
			if ( result.getId()  == resultId ) {
				return result;
			}
		}
		return null;
	}
}

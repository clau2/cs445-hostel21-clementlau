import static org.junit.Assert.*;

import org.junit.Test;


public class CustomerTest {
	
	private Customer cWithCCInfo, cWithoutCCInfo;
	
	public CustomerTest () {
		cWithCCInfo = new Customer();
		cWithoutCCInfo = new Customer();
		cWithCCInfo.setFirstName("Clement");
		cWithCCInfo.setLastName("Lau");
		cWithCCInfo.setEmail("clau2@hawk.iit.edu");
		cWithCCInfo.setCCNumber(1234);
		cWithCCInfo.setExpirationDate(1234);
		cWithCCInfo.setSecurityCode(1234);
		cWithoutCCInfo.setFirstName("Clement");
		cWithoutCCInfo.setLastName("Lau");
		cWithoutCCInfo.setEmail("clau2@hawk.iit.edu");
	}

	@Test
	public void testHasCCInfo() {
		assertTrue(cWithCCInfo.hasCCInfo());
		assertFalse(cWithoutCCInfo.hasCCInfo());
	}

}

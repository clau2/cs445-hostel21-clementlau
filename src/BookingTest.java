import static org.junit.Assert.*;

import org.junit.Test;


public class BookingTest {
	
	private Availability a;
	private SearchResult s1, s2;
	private Booking b1, b2;
	private Customer cWithCCInfo, cWithoutCCInfo;
	
	public BookingTest () {
		a = new Availability();
		s1 = new SearchResult();
		s2 = new SearchResult();
		b1 = new Booking();
		b2 = new Booking();
		cWithCCInfo = new Customer();
		cWithoutCCInfo = new Customer();
		s1.addAvailability(a);
		s2.addAvailability(a);
		b1.setBeds(s1);
		b2.setBeds(s2);
		cWithCCInfo.setFirstName("Clement");
		cWithCCInfo.setLastName("Lau");
		cWithCCInfo.setEmail("clau2@hawk.iit.edu");
		cWithCCInfo.setCCNumber(1234);
		cWithCCInfo.setExpirationDate(1234);
		cWithCCInfo.setSecurityCode(1234);
		cWithoutCCInfo.setFirstName("Clement");
		cWithoutCCInfo.setLastName("Lau");
		cWithoutCCInfo.setEmail("clau2@hawk.iit.edu");
	}

	@Test
	public void testReserve() {
		try {
			b1.reserve(cWithCCInfo);
		} catch ( Exception e ) {
			fail("Should not have thrown error");
		}
		assertEquals(cWithCCInfo, b1.getReservee());
		
		try {
			b1.reserve(cWithCCInfo);
			fail("Should have thrown error because this was already booked");
		} catch ( Exception e ) {}
		
		
		try {
			b2.reserve(cWithoutCCInfo);
			fail("Should have thrown error because customer is missing CC info");
		} catch (Exception e) {}
	}

	@Test
	public void testCancel() throws Exception {
		b1.reserve(cWithCCInfo);
		try {
			b1.cancel();
			b1.reserve(cWithCCInfo);
		} catch ( Exception e ) {
			fail("Should not have thrown error");
		}
	}

}


public class Booking {
	private SearchResult beds;
	private Customer reservee;
	private int id;
	private static int idCounter = 1;
	
	public Booking () {
		id = idCounter++;
	}
	
	public void setBeds ( SearchResult e ) {
		beds = e;
	}
	
	public void reserve ( Customer e ) throws Exception {
		if ( !e.hasCCInfo() ) {
			throw new Exception("this customer has not registered credit card info");
		}
		reservee = e;
		beds.book(e);
	}
	
	public void cancel () {
		beds.cancel();
	}
	
	public SearchResult getBeds () {
		return beds;
	}
	
	public Customer getReservee () {
		return reservee;
	}
	
	public int getId () {
		return id;
	}
	
	public String toString () {
		String a = "";
		a += beds.getHostel().getName() + ", " + beds.getHostel().getCity() + "\n";
		a += "Check-in date: " + beds.startDate() + "\n";
		a += "Check-out date: " + beds.endDate() + "\n";
		a += "Beds: " + beds.getNum() + "\n";
		a += "Booking ID: " + id + "\n";
		a += "Name: " + reservee.getName() + "\n";
		a += "Price: $" + beds.getPrice();
		return a;
	}
}

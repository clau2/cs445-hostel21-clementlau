import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Scanner;


public class Hostel21 {
	private ArrayList <Hostel> hostels;
	
	private static ArrayList <Customer> customers;
	private static ArrayList <Booking> bookings;
	
	private static int revenue;
	
	public Hostel21 () {
		this.hostels = new ArrayList <Hostel> ();
	}
	
	public void init () {
		SearchEngine.init(this);
		customers = new ArrayList <Customer> ();
		bookings = new ArrayList <Booking> ();
		revenue = 0;
		return;
	}
	
	private static Customer findCustomer ( int id ) throws Exception {
		ListIterator <Customer> li = customers.listIterator();
		while ( li.hasNext() ) {
			Customer c = li.next();
			if ( c.getId() == id ) {
				return c;
			}
		}
		throw new Exception("invalid customer id");
	}
	
	private static Booking findBooking ( int id ) throws Exception {
		ListIterator <Booking> li = bookings.listIterator();
		while ( li.hasNext() ) {
			Booking b = li.next();
			if ( b.getId() == id ) {
				return b;
			}
		}
		throw new Exception("invalid booking id");
	}
	
	public static void cancel ( SearchResult s ) {
		
	}
	
	public Hostel addHostelIfItDoesNotAlreadyExists ( String city, String name, String address ) {
		if ( !this.hostels.contains(new Hostel(name)) ) {
			Hostel newlyAddedHostel = new Hostel(city,name,address);
			this.hostels.add(newlyAddedHostel);
			return newlyAddedHostel;
		} else {
			return this.searchHostel(city);
		}
	}
	
	public Hostel searchHostel ( String city ) {
		ListIterator <Hostel> li = hostels.listIterator();
		while ( li.hasNext() ) {
			Hostel possibleResult = li.next();
			if ( possibleResult.equals(new Hostel(city)) ) {
				return possibleResult;
			}
		}
		return null;
	}
	
	public static void main ( String args[] ) throws Exception {
		Hostel21 h21 = new Hostel21();
		h21.init();
		XMLParser.init(h21);
		prompt();
	}
	
	public static void prompt () throws Exception {
		Scanner sc = new Scanner ( System.in );
		while ( true ) {
			System.out.print("\n$ ");
			String input = sc.nextLine();
			String [] tokenizedInput = input.split(" (?=([^\"]*\"[^\"]*\")*[^\"]*$)");
			try {
				if ( tokenizedInput[0].equals("quit") || tokenizedInput[1].equals("quit") ) {
					sc.close();
					return;
				}
				else if ( tokenizedInput[1].equals("search") ) {
					parseSearch(tokenizedInput);
				}
				else if ( tokenizedInput[1].equals("admin") ) {
					parseAdmin(tokenizedInput);
				}
				else if ( tokenizedInput[1].equals("user") ) {
					parseUser(tokenizedInput);
				}
				else if ( tokenizedInput[1].equals("book") ) {
					parseBook(tokenizedInput);
				}
				else {
					System.out.println("\"" + input + "\" is bad input");
				}
			} catch ( Exception e ) {
				System.out.println("ERROR!!");
				System.out.println(e.getMessage());
			}
		}
	}
	
	public static void parseSearch ( String [] input ) throws Exception {
		int startDate = 0;
		int endDate = 0;
		int beds = 0;
		ArrayList <SearchResult> result;
		String city = "";
		for ( int i = 2; i < input.length-1; i++ ) {
			if ( input[i].equals("--start_date") ) {
				startDate = Integer.parseInt(input[++i]);
			}
			else if ( input[i].equals("--end_date") ) {
				endDate = Integer.parseInt(input[++i]);
			}
			else if ( input[i].equals("--beds") ) {
				beds = Integer.parseInt(input[++i]);
			}
			else if ( input[i].equals("--city") ) {
				city = input[++i].split("\"")[1];
			}
		}
		if ( startDate == 0 ) {
			throw new SearchException("missing start date");
		}
		if ( city.equals("") ) {
			throw new SearchException("missing city");
		}
		if ( endDate == 0 ) {
			throw new SearchException("missing end date");
		}
		
		if ( beds > 0 ) {
			result = SearchEngine.search(city, beds, startDate, endDate);
			displayResults(result);
		} else {
			result = SearchEngine.checkAvailability(city, startDate, endDate);
			displayAvailability(result, startDate, endDate);
		}
	}
	
	public static void parseAdmin ( String [] input ) throws Exception {
		if ( input[2].equals("load") ) {
			adminLoad(input);
		} else if ( input[2].equals("revenue") ) {
			adminRevenue(input);
		} else if ( input[2].equals("occupancy") ) {
			adminOccupancy(input);
		} else {
			throw new Exception("wrong admin command");
		}
	}
	
	public static void parseUser ( String [] input ) throws Exception {
		if ( input[2].equals("add") ) {
			userAdd(input);
		} else if ( input[2].equals("change") ) {
			userChange(input);
		} else if ( input[2].equals("view") ) {
			userView(input);
		} else {
			throw new Exception("wrong user command");
		}
	}
	
	public static void parseBook ( String [] input ) throws Exception {
		if ( input[2].equals("add") ) {
			bookAdd(input);
		} else if ( input[2].equals("cancel") ) {
			bookCancel(input);
		} else {
			throw new Exception("wrong book command");
		}
	}
	
	public static void adminLoad ( String [] input ) throws Exception {
		XMLParser.loadFromXML(input[3].split("\"")[1]);
	}
	
	public static void adminRevenue ( String [] input ) {
		System.out.println("We have made $" + revenue + "!!");
	}
	
	public static void adminOccupancy ( String[] input ) {
		
	}
	
	public static void userAdd ( String[] input ) throws Exception {
		Customer customer = parseUserAdd(input);
		ListIterator <Customer> li = customers.listIterator();
		while ( li.hasNext() ) {
			Customer c = li.next();
			if ( c.getEmail().equals(customer.getEmail()) ) {
				throw new Exception("this email is alrealdy used");
			}
		}
		customers.add(customer);
		System.out.println(customer.toString());
	}
	
	public static Customer parseUserAdd ( String[] input ) throws Exception {
		String firstName = "";
		String lastName = "";
		String email = "";
		int ccNumber = 0;
		int expiration = 0;
		int security = 0;
		int phone = 0;
		for ( int i = 3; i < input.length-1; i++ ) {
			if ( input[i].equals("--first_name") ) {
				firstName = input[++i];
			} else if ( input[i].equals("--last_name") ) {
				lastName = input[++i];
			} else if ( input[i].equals("--email") ) {
				email = input[++i];
			} else if ( input[i].equals("--cc_number") ) {
				ccNumber = Integer.parseInt(input[++i]);
			} else if ( input[i].equals("--expiration_date") ) {
				expiration = Integer.parseInt(input[++i]);
			} else if ( input[i].equals("--security_code") ) {
				security = Integer.parseInt(input[++i]);
			} else if ( input[i].equals("--phone") ) {
				phone = Integer.parseInt(input[++i]);
			}
		}
		if ( firstName.equals("") ) {
			throw new Exception("missing first name");
		}
		if ( lastName.equals("") ) {
			throw new Exception("missing last name");
		}
		if ( email.equals("") ) {
			throw new Exception("missing email");
		}
		Customer customer = new Customer ();
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setEmail(email);
		customer.setCCNumber(ccNumber);
		customer.setExpirationDate(expiration);
		customer.setSecurityCode(security);
		customer.setPhone(phone);
		return customer;
	}
	
	public static void userChange ( String[] input ) throws Exception {
		Customer customer = parseUserChange(input);
		System.out.println(customer.toString());
	}
	
	public static Customer parseUserChange ( String[] input ) throws Exception {
		int id = 0;
		String firstName = "";
		String lastName = "";
		String email = "";
		int ccNumber = 0;
		int expiration = 0;
		int security = 0;
		int phone = 0;
		for ( int i = 3; i < input.length-1; i++ ) {
			if ( input[i].equals("--user_id") ) {
				id = Integer.parseInt(input[++i]);
			} else if ( input[i].equals("--first_name") ) {
				firstName = input[++i];
			} else if ( input[i].equals("--last_name") ) {
				lastName = input[++i];
			} else if ( input[i].equals("--email") ) {
				email = input[++i];
			} else if ( input[i].equals("--cc_number") ) {
				ccNumber = Integer.parseInt(input[++i]);
			} else if ( input[i].equals("--expiration_date") ) {
				expiration = Integer.parseInt(input[++i]);
			} else if ( input[i].equals("--security_code") ) {
				security = Integer.parseInt(input[++i]);
			} else if ( input[i].equals("--phone") ) {
				phone = Integer.parseInt(input[++i]);
			}
		}
		if ( id == 0 ) {
			throw new Exception("missing user id");
		}
		Customer customer = findCustomer(id);
		if ( !firstName.equals("") ) {
			customer.setFirstName(firstName);
		}
		if ( !lastName.equals("") ) {
			customer.setLastName(lastName);
		}
		if ( !email.equals("") ) {
			customer.setEmail(email);
		}
		if ( ccNumber != 0 ) {
			customer.setCCNumber(ccNumber);
		}
		if ( expiration != 0 ) {
			customer.setExpirationDate(expiration);
		}
		if ( security != 0 ) {
			customer.setSecurityCode(security);
		}
		if ( phone != 0 ) {
			customer.setPhone(phone);
		}
		return customer;
	}
	
	public static void userView ( String [] input ) throws Exception {
		Customer customer = parseUserView(input);
		System.out.println(customer.toString());
	}
	
	public static Customer parseUserView ( String [] input ) throws Exception {
		int id = 0;
		for ( int i = 3; i < input.length-1; i++ ) {
			if ( input[i].equals("--user_id") ) {
				id = Integer.parseInt(input[++i]);
			}
		}
		if ( id == 0 ) {
			throw new Exception("no user id provided");
		}
		Customer customer = findCustomer(id);
		return customer;
	}
	
	public static void bookAdd ( String [] input ) throws Exception {
		Booking b = parseBookAdd(input);
		bookings.add(b);
		revenue += b.getBeds().getPrice();
		System.out.println("Booking successful! Here's the detail of your booking:");
		System.out.println(b.toString());
	}
	
	public static Booking parseBookAdd ( String [] input ) throws Exception {
		int userId = 0;
		int searchId = 0;
		for ( int i = 3; i < input.length-1; i++ ) {
			if ( input[i].equals("--user_id") ) {
				userId = Integer.parseInt(input[++i]);
			} else if ( input[i].equals("--search_id") ) {
				searchId = Integer.parseInt(input[++i]);
			}
		}
		if ( userId == 0 ) {
			throw new Exception("missing user id");
		}
		if ( searchId == 0 ) {
			throw new Exception("missing search id");
		}
		Booking booking = new Booking ();
		booking.setBeds(SearchEngine.getResult(searchId));
		booking.reserve(findCustomer(userId));
		return booking;
	}
	
	public static void bookCancel ( String[] input ) throws Exception {
		Booking b = parseBookCancel(input);
		b.cancel();
		if ( b.getBeds().getHostel().isTooLateToCancel(b.getBeds()) ) {
			revenue -= (100-b.getBeds().getHostel().getCancellationFee()) * b.getBeds().getPrice() / 100;
			System.out.println("Unfortunately, you will be charged a cancellation fee for cancelling too late.");
		} else {
			revenue -= b.getBeds().getPrice();
			System.out.println("You have been refunded your monies");
		}
		bookings.remove(b);
		System.out.println("Booking has been cancelled :(");
	}
	
	public static Booking parseBookCancel ( String [] input ) throws Exception {
		int bookingId = 0;
		for ( int i = 3; i < input.length-1; i++ ) {
			if ( input[i].equals("--booking_id") ) {
				bookingId = Integer.parseInt(input[++i]);
			}
		}
		if ( bookingId == 0 ) {
			throw new Exception("missing booking id");
		}
		Booking booking = findBooking(bookingId);
		return booking;
	}
	
	public static void displayResults ( ArrayList <SearchResult> result ) {
		if ( result.isEmpty() ) {
			System.out.println("Sorry, not enough beds");
			return;
		}
		ListIterator <SearchResult> li = result.listIterator();
		while ( li.hasNext() ) {
			System.out.println(li.next().toString());
		}
	}
	
	public static void displayAvailability ( ArrayList <SearchResult> result, int startDate, int endDate ) {
		ListIterator <SearchResult> li = result.listIterator();
		for ( int date = startDate; date < endDate; date = SearchEngine.nextDay(date) ) {
			System.out.print(date + " to " + SearchEngine.nextDay(date) + ": ");
			SearchResult r = li.next();
			if ( r.isEmpty() ) {
				System.out.println("no beds available");
			} else if ( r.size() == 1 ) {
				System.out.println("1 bed available at $" + r.getPrice());
			} else {
				System.out.println(r.size() + " beds available from $" + r.minPrice() + " to $" + r.maxPrice());
			}
		}
	}
}

import static org.junit.Assert.*;

import org.junit.Test;


public class RoomTest {
	
	private Room r;
	private Availability a1, a2;
	private SearchResult s1, s2;
	
	public RoomTest () {
		r = new Room();
		a1 = new Availability();
		a2 = new Availability();
		s1 = new SearchResult();
		s2 = new SearchResult();

		a1.setDate(20140701);
		a2.setDate(20140702);
		r.addAvailability(a1);
		r.addAvailability(a2);
		s1.addAvailability(a1);
		s2.addAvailability(a1);
		s2.addAvailability(a2);
	}

	@Test
	public void testAddBedIfItDoesNotAlreadyExist() {
		assertNull(r.getBed(1));
		r.addBedIfItDoesNotAlreadyExist(1);
		assertNotNull(r.getBed(1));
	}

	@Test
	public void testSearch() {
		assertTrue(r.search(20140701, 20140702).contains(s1));
		assertTrue(r.search(20140701, 20140703).contains(s2));
	}

	@Test
	public void testCheckAvailability() {
		Room r = new Room();
		Availability a1 = new Availability(20,20140701,1,1);
		Availability[] a = {};
		assertArrayEquals(r.checkAvailability(20140701).getResults().toArray(),a);
		r.addAvailability(a1);
		assertEquals(r.checkAvailability(20140701).getResults().get(0),a1);
	}

}

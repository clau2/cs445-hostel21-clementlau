
public class Customer {
	private String firstName;
	private String lastName;
	private String email;
	private int phone;
	private int ccNumber;
	private int expirationDate;
	private int securityCode;
	private int id;
	private static int idCounter = 1;
	
	public Customer () {
		this.firstName = null;
		this.lastName = null;
		this.email = null;
		this.phone = 0;
		this.ccNumber = 0;
		this.expirationDate = 0;
		this.securityCode = 0;
		this.id = idCounter++;
	}
	
	public void setFirstName ( String name ) {
		this.firstName = name;
	}
	
	public void setLastName ( String name ) {
		this.lastName = name;
	}
	
	public void setEmail ( String email ) {
		this.email = email;
	}
	
	public void setPhone ( int phoneNumber ) {
		this.phone = phoneNumber;
	}
	
	public void setCCNumber ( int ccNumber ) {
		this.ccNumber = ccNumber;
	}
	
	public void setExpirationDate ( int date ) {
		this.expirationDate = date;
	}
	
	public void setSecurityCode ( int code ) {
		this.securityCode = code;
	}
	
	public String getFirstName () {
		return this.firstName;
	}
	
	public String getLastName () {
		return this.lastName;
	}
	
	public String getEmail () {
		return this.email;
	}
	
	public int getPhone () {
		return this.phone;
	}
	
	public int getCCNumber () {
		return this.ccNumber;
	}
	
	public int getExpirationDate () {
		return this.expirationDate;
	}
	
	public int getSecurityCode () {
		return this.securityCode;
	}
	
	public int getId () {
		return this.id;
	}
	
	public String getName () {
		return firstName + " " + lastName;
	}
	
	public String toString () {
		String a = "";
		a += "user_id: ";
		a += id;
		a += "\nName: ";
		a += firstName + " " + lastName;
		a += "\nEmail: ";
		a += email;
		if ( ccNumber != 0 ) {
			a += "\nCredit card number: " + ccNumber;
		}
		if ( expirationDate != 0 ) {
			a += "\nExpiration date: " + expirationDate;
		}
		if ( securityCode != 0 ) {
			a += "\nSecurity code: " + securityCode;
		}
		if ( phone != 0 ) {
			a += "\nPhone number: " + phone;
		}
		return a;
	}
	
	public boolean hasCCInfo () {
		if ( ccNumber == 0 ) {
			return false;
		}
		if ( expirationDate == 0 ) {
			return false;
		}
		if ( securityCode == 0 ) {
			return false;
		}
		return true;
	}
}
